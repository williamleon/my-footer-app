import React from "react";
import './styles.css';

export default function Root(props) {

  return (
    <div>
      <footer className="footer">
        <div className="footer-logo"></div>
        <p className="footer-text">MicroFrontEnds Demo</p>
      </footer>
    </div>
    );
}
